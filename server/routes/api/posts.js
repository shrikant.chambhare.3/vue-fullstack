const express = require('express');
const mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
const router = express.Router();
// Get post
router.get('/', async (req, res) => {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        dbo = db.db("mydb");
        dbo.collection("forms").find({}).toArray(
            function(err, result) {
                if (err) throw err;
                res.send(result);
                db.close();
              }
        );
        db.close();
      }); 
});


//add post
router.post('/', async (req, res) => {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        dbo = db.db("mydb");
        dbo.collection("forms").insertOne({
            text: req.body.text,
            createdAt: new Date()
        });
        res.status(201).send();
        db.close();
      }); 
});

// Delete post
router.delete('/:id', async (req, res) => {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        dbo = db.db("mydb");
        dbo.collection("forms").deleteOne({ _id: new mongodb.ObjectID(req.params.id)}, function(err, obj) {
            if (err) throw err;
            console.log(obj.result.n + " document(s) deleted");
            db.close();
        });
        res.status(202).send();
        db.close();
      }); 
});


module.exports = router;

